#include "../include/files.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include <malloc.h>
#include <stdio.h>



char* Errors[] = {"Success read","Error bm bytes", "Error count bits", "Error size", "Error compression", "Reading error" };

int main(int argc, char* argv[]) {
    if(argc >= 3){
        FILE* Source = fopen(argv[1], "rb");
        FILE* New = fopen(argv[2], "wb");

        if(Source == NULL || New == NULL) {
            printf("file not found");
            return -1;
        }

        struct Image* image = malloc(sizeof(struct Image));
        printf("%s\n", Errors[load_bmp(Source, image)]);

        struct Image* new = rotate(image);

        create_bmp(New, new);

        fclose(Source);
        fclose(New);

        Clear(image);
        Clear(new);

        return 0;
    }
    else{
        printf("count args not 3 and more");
        return -4;
    }

}
