#include "../include/image.h"
#include <malloc.h>

void new_memory (struct Image* image, unsigned long width, unsigned long height){
    image->width = width;
    image->height = height;
    image->colors = (struct Color*)malloc(sizeof(struct Color) * width * height);
}
void Clear(struct Image* image){
    free(image->colors);
    free(image);
}
