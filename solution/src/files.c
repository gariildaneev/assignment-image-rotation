#include "../include/files.h"
#include "../include/image.h"
#include <malloc.h>
#include <stdio.h>
#define BI_SIZE 0
#define UNUSED 0
#define BI_OFFSET 54
#define SIZE 40
#define WIDTH 0
#define HEIGHT 0
#define PLANES 1
#define BITS 24
#define COMP 0
#define IM_SIZE 0
#define XPM 0
#define YPM 0
#define CLR_USED 0
#define IND_CLR 0


int zero_pointer = 0;
struct Header header = {"BM", BI_SIZE, UNUSED,  BI_OFFSET, SIZE, WIDTH, HEIGHT, PLANES, BITS, COMP, IM_SIZE, XPM, YPM, CLR_USED, IND_CLR};

Status header_check(struct Header* header, size_t size, struct Header* headers){
            if(headers->biOffset != header->biOffset || headers->sizeHeader != header->sizeHeader || headers->biSize != size)
                return ERROR_SIZE;
            else if(headers->BM[0] != 'B' || headers->BM[1] != 'M')
                return BM;
            else if(headers->bits != header->bits)
                return BITS;
            else if(headers->compression != header->compression)
                return COMP;
            else return OK;    
        }

uint32_t padding_value(struct Image* image){
    return (4- ((3 * image->width) % 4)) % 4;
}

Status load_bmp(FILE* Source, struct Image* New){
    struct Header headers;

    if (fseek(Source, 0L, SEEK_END)){
        return READ_ERROR;
    }
    
    size_t size = ftell(Source);
    rewind(Source);

    if (fread(&headers, sizeof(header), 1, Source) != 1) return READ_ERROR;

    if (header_check(&header, size, &headers) != OK) return header_check(&header, size, &headers);


    new_memory(New, headers.width, headers.height);
    uint32_t padding = padding_value(New);
    for (size_t i = 0; i < headers.height; ++i) {
        if(fread(&New->colors[i * New->width], sizeof(struct Color), New->width, Source) != New->width) return READ_ERROR;
        if (fseek(Source, padding, SEEK_CUR) != 0){
            return READ_ERROR;
        }
    }
    return OK;
}


Status create_bmp(FILE* file, struct Image* image) {
    uint32_t padding = padding_value(image);
    uint32_t size = BI_OFFSET + image->width * image->height * 3 + image->height * padding;
    fwrite(&header.BM, sizeof(uint8_t) * 2, 1, file);
    fwrite(&size, sizeof(uint32_t), 1, file);
    fwrite(&header.Unused, sizeof(uint32_t), 1, file);
    fwrite(&header.biOffset, sizeof(uint32_t), 1, file);
    fwrite(&header.sizeHeader, sizeof(uint32_t), 1, file);
    fwrite(&image->width, sizeof(uint32_t), 1, file);
    fwrite(&image->height, sizeof(uint32_t), 1, file);
    fwrite(&header.planes, sizeof(uint16_t), 1, file);
    fwrite(&header.bits, sizeof(uint16_t), 1, file);
    fwrite(&header.compression, sizeof(uint32_t), 1, file);
    size -= BI_OFFSET;
    fwrite(&header.sizeImage, sizeof(uint32_t), 1, file);
    fwrite(&header.xPerMeter, sizeof(uint32_t), 1, file);
    fwrite(&header.yPerMeter, sizeof(uint32_t), 1, file);
    fwrite(&header.clrUsed, sizeof(uint32_t), 1, file);
    fwrite(&header.indColor, sizeof(uint32_t), 1, file);



    for (size_t x = 0; x < image->height; ++x) {
        if (fwrite(&image->colors[x * image->width], sizeof(struct Color), image->width, file) != image->width) return WRITE_ERROR;
        if (fwrite(&zero_pointer, 1, padding, file) != padding) return WRITE_ERROR;
    }
    return OK;
}
