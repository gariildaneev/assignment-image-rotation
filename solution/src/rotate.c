#include "../include/image.h"
#include "../include/rotate.h"
#include "stdlib.h"

struct Image* rotate(struct Image* const src){
    uint32_t width = src->width;
    uint32_t height = src->height;

    struct Image* new = malloc(sizeof(struct Image));
    new_memory(new, height, width);

    for (uint64_t i = 0; i < width; i++) {
        for (uint64_t j = 0; j < height; j++) {
            new->colors[i * height + j] =  src->colors[(height - 1 - j) * width + i];
        }
    }

    return new;
}
