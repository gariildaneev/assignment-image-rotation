#include "structures.h"

Status load_bmp(FILE* Source, struct Image* New);
Status create_bmp(FILE* file, struct Image* image);

Status header_check (struct Header* header, size_t size, struct Header* headers);
uint32_t padding_value(struct Image* image);

