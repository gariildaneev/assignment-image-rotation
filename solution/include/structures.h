#pragma once

#include <stdint.h>
#include <stdio.h>


#pragma pack(push, 1)
struct Color{
    uint8_t b, g, r;
} ;
#pragma pack(pop)

typedef enum Status{
    OK,
    BM,
    BITS,
    ERROR_SIZE,
    COMP,
    READ_ERROR,
    WRITE_ERROR
} Status;

#pragma pack(push, 1)
struct Image{
    uint32_t width, height;
    struct Color* colors;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct Header{
    unsigned char BM[2];
    uint32_t biSize;
    uint32_t Unused;
    uint32_t biOffset;
    uint32_t sizeHeader;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bits;
    uint32_t compression;
    uint32_t sizeImage;
    uint32_t xPerMeter;
    uint32_t yPerMeter;
    uint32_t clrUsed;
    uint32_t indColor;
};
#pragma pack(pop)


